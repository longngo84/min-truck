import React from 'react';

import './App.css';
import TruckFleet from './components/truck-fleet';
import Schedule from './components/schedule';
import { convertSchedule } from './services/helpers';
import inputData from './input.json'
import { getSchedule } from './services/min-truck';

const input = convertSchedule(inputData)
const mockData = getSchedule(inputData)

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <div className='min-truck-app'>
          <div>Input:</div>
          <Schedule data={input} />
          <div>&nbsp;</div>
          <div>Result:</div>
          <TruckFleet data={mockData} />
        </div>
      </header>
    </div>
  );
}

export default App;
