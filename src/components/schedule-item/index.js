import React from 'react'
import './style.css'

export default function ScheduleItem({start, end}) {
    const display = (value) => value < 10 ? `0${value}` : value

    const format = (value) => {
        if (value < 100) {
            return `00:${value}`
        }

        return `${display(Math.floor(value/100))}:${display(value % 100)}`
    }

    return (
        <li className='schedule-item'>
            {format(start)} - {format(end)}
        </li>
    )
}
