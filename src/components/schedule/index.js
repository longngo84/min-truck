import React from 'react'
import ScheduleItem from '../schedule-item'
import './style.css'

export default function Schedule({ data }) {
    return (
        <ul className='schedule-item-container'>
            {data.map((item, idx) => {
                return <ScheduleItem key={idx} start={item.start} end={item.end} />
            })}
        </ul>
    )
}
