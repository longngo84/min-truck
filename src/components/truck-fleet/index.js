import React from 'react'
import Truck from '../truck'
import './style.css'

export default function TruckFleet({ data }) {
    return (
        <div className='truck-fleet-container'>
            <div>Truck fleet count: {data.length}</div>
            <ul>
                {data.map((item, idx) => {
                    const { name, schedule } = item

                    return <Truck key={idx} name={name} schedule={schedule} />
                })}
            </ul>
        </div>
    )
}
