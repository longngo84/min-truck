import React from 'react'
import Schedule from '../schedule'
import './style.css'

export default function Truck({ name, schedule }) {
    return (
        <li className='truck-container'>
            <span className='truck-name'>Truck {name} ({schedule.length}):</span>
            <span className='truck-schedule'>
                <Schedule data={schedule} />
            </span>
        </li>
    )
}
