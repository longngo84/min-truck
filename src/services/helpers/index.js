export const convertSchedule = (input) => {
    return input.map(item => {
        return { start: item[0], end: item[1] }
    })
}