import { convertSchedule } from '../helpers'

export const getSchedule = (inputData) => {
    const sortedInput = inputData.sort((a, b) => a[0] - b[0])

    const input = convertSchedule(sortedInput)

    const result = [
        {
            name: '1',
            schedule: [ input[0] ]
        }
    ]

    for (let idx = 1; idx < input.length; idx++) {
        const item = input[idx];
        let foundTruck = false

        for (let trkIdx = 0; trkIdx < result.length; trkIdx++) {
            const truck = result[trkIdx];
            
            const lastEndTime = truck.schedule[truck.schedule.length - 1].end

            if (item.start >= lastEndTime) {
                truck.schedule.push(item)
                foundTruck = true
                break
            }
        }

        if (!foundTruck) {
            result.push({
                name: (1 + idx).toString(),
                schedule: [ item ]
            })
        }
    }

    return result
}

